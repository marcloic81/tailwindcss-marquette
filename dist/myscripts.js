function draw() {
    var canvas1 = document.getElementById('circle1');
    if (canvas1.getContext) {
        var circle1 = canvas1.getContext('2d'); 
        var X = canvas1.width / 2;
        var Y = canvas1.height / 2;
        var R = 150;
        circle1.beginPath();
        circle1.arc(X, Y, R, 0, 2 * Math.PI, false);
        circle1.lineWidth = 30;
        circle1.strokeStyle = '#F8C600';
        circle1.stroke();
    }
    var canvas2 = document.getElementById('circle2');
    if (canvas2.getContext) {
        var circle2 = canvas2.getContext('2d'); 
        var X = canvas2.width / 2;
        var Y = canvas2.height / 2;
        var R = 150;
        circle2.beginPath();
        circle2.arc(X, Y, R, 0, 2 * Math.PI, false);
        circle2.lineWidth = 30;
        circle2.strokeStyle = '#F8C600';
        circle2.stroke();
    }
    var c = document.getElementById("line1");
    var line1 = c.getContext("2d");
    line1.beginPath();
    line1.moveTo(100, 100);
    line1.lineTo(170, 150);
    line1.strokeStyle = '#00000';
    line1.stroke();
}

