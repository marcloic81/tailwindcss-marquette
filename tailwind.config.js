module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      margin: {
        sm: '8px',
        md: '16px',
        lg: '24px',
        xl: '300px',
        nl1: '1270px',
        nl2: '1260px',
        nl3: '1270px',
        nl4: '1245px'
      },
      listStyleType: {
        none: 'none',
       disc: 'disc',
       decimal: 'decimal',
       square: 'square',
       roman: 'upper-roman',
      },
      width: {
        sm: '50px',
        lg: '70px',
        mg: '50px',
        sg: '100px'
      },
      height: {
        sm: '570px',
        lg: '850px',
        mg: '450px',
        sg: '800px'
      },
      outline: {
        blue: '1px solid white',
      },
      backgroundImage: {
        'background-1': "url('../images/5.jpg')",
        'background-2': "url('../images/world_map.jpg')",
    },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
